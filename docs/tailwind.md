[docs](https://tailwindcss.com/docs/guides/laravel)
```bash
# install laravel 8, not 9
composer create-project laravel/laravel="8.*" tailwindcss-starter
```